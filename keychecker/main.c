#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

// idea: use gdb to see that exactly one cmd argument with 'x' as 9th letter and total length 0 mod 4 has to be passed to achieve anything...

int main(int argc, char** argv)
{
	if(argc==2) {
		if(argv[1][9]=='x') {
			if(strlen(argv[1])%4 == 0) {
				printf("yay! your flag is: ");
				fflush(stdout);
				char* call = (char*) malloc(sizeof(char)*strlen(argv[1]+12));
				strcpy(call, "/var/verifyer ");
				strcat(call, argv[1]);
				int x = system(call);
			}
		}
	}
	return 0;
}
