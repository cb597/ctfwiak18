SUBDIRS = brokenheader keychecker buffer findfunction stripped demo inmemory

all :
	rm -rf bin
	mkdir bin
	for d in $(SUBDIRS); do (cd $$d; make; cd ..); done
	cp keychecker/task bin/1
	sudo cp keychecker/verifyer /var/verifyer
	sudo chmod -r /var/verifyer
	cp brokenheader/task bin/2
	cp buffer/task bin/3
	cp findfunction/task bin/4
	cp inmemory/task bin/5
	cp stripped/task bin/6
