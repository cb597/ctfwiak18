#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

void segv_handler(int c){
	int f[] = {87, 49, 51, 110, 51, 114, 53, 99, 104, 110, 49, 116, 122, 51, 108, 10};
	for(int i=0; i<17; ++i)
	putc(f[i], stdout);
	exit(0);
}

int main(int argc, char** argv)
{
	signal(SIGSEGV, segv_handler);
	if(argc==1){
		printf("no arguments given\n");
		return 0;
	}
	char pw[16];
	strcpy(pw, argv[1]);
	printf("%s\n", pw);
	return 0;
}
